import {Fragment, useState, useEffect, useContext} from "react";
import {Form, Button} from "react-bootstrap";
import Swal from "sweetalert2";
import {Redirect, useHistory} from "react-router-dom";
import UserContext from "../UserContext";

export default function Register(){
	//State hooks to store the values of the input fields
	const [fName, setFName] = useState("")
	const [lName, setLName] = useState("")
	const [email, setEmail] = useState("");
	const [phoneNum, setPhoneNum] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const {user,setUser} = useContext(UserContext);
	const history = useHistory();

	const [isActive, setIsActive] = useState(false);

	//the e.target.value property allows us to gain access to the inputfield's current value to be used when submitting form data

	//check if values are successfully bound
	// console.log(email)
	// console.log(password1)
	// console.log(password1)

	useEffect(()=>{
		//Validation to enable submit button when all fields are populated and both passwords match
		if((password1 === password2) && (fName !== "" && lName !== "" && email !== "" && password1 !== "" && password2 !== "" && phoneNum !== "") && (phoneNum.length >= 11)){
				setIsActive(true);
		}else{
				setIsActive(false);
		}
	},[email, password1, password2])

	function registerUser(e){
		e.preventDefault();
		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data === true){
				Swal.fire({
					title: 'Duplicate email exists!',
					icon: 'error',
					text: `${email} is already taken. Please choose another email.`,
				})
			}else{
				fetch("http://localhost:4000/users/register", {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					firstName: fName,
					lastName: lName,
					email: email,
					mobileNo: phoneNum,
					password: password1,
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)
				if(data === true){
					Swal.fire({
						title: "Registration successful!",
						icon: "success",
						text: `${fName} ${lName} has registered.`
					})
					history.push("/login")
				}else{
					Swal.fire({
						title: "Oooops!",
						icon: "error",
						text: "Something went wrong. Check your Credentials"
					})
				}
				
			})


			}
		})
			
				setFName("")
				setLName("")
				setEmail("")
				setPhoneNum("")
				setPassword1("")
				setPassword2("")
	}




	//Two-way binding
	//The values in the fields of the form is bound to the getter of the state and the event is bound to the setter. This is called two-way binding
	//The data we changed in the view has updated the state
	//The data in the state has updated the view

	return(
		(user.accessToken !== null)
		? <Redirect to="/" />
		: <Fragment>
		<h1>Register</h1>
		<Form onSubmit={e=>registerUser(e)}>
			<Form.Group>
				<Form.Label>First Name:</Form.Label>
				<Form.Control type="text" placeholder="Enter first name" required value={fName} 
				onChange={e=>setFName(e.target.value)} />
			</Form.Group>

			<Form.Group>
				<Form.Label>Last Name:</Form.Label>
				<Form.Control type="text" placeholder="Enter last name" required value={lName} 
				onChange={e=>setLName(e.target.value)} />
			</Form.Group>

			<Form.Group>
				<Form.Label>Email address:</Form.Label>
				<Form.Control type="email" placeholder="Enter email" required value={email} 
				onChange={e=>setEmail(e.target.value)} />
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>Mobile Number:</Form.Label>
				<Form.Control type="text" placeholder="Enter mobile number" required value={phoneNum} 
				onChange={e=>setPhoneNum(e.target.value)} />
			</Form.Group>

			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Enter your Password" required value={password1}
				 onChange={e=>setPassword1(e.target.value)} />
			</Form.Group>

			<Form.Group>
				<Form.Label>Verify Password:</Form.Label>
				<Form.Control type="password" placeholder="Verify your Password" required value={password2}
				onChange={e=>setPassword2(e.target.value)} />
			</Form.Group>

			{isActive
			? <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
			: <Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
			}
		</Form>
		</Fragment>
		)
}

//Form.Control = Input 
