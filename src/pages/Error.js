import {Link} from "react-router-dom";
import {Container, Jumbotron} from "react-bootstrap";

export default function Error(){
	return(
		<Container>
			<Jumbotron>
				<img alt="404" width="100%" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSe1XsjUbzJNUFKCT2oLt7TE-ZeQCPgLuApgnLVkXbi-Agqvy4DsflC60Amx47LvYCD7Ew&usqp=CAU" />
				<p>Go back to the <Link as={Link} to="/">Home</Link></p>
			</Jumbotron>
		</Container>
		)
}