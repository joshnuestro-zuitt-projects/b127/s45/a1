import {Fragment, useState, useEffect, useContext} from "react";
import {Form, Button} from "react-bootstrap";
import Swal from "sweetalert2";
//React Context
import UserContext from "../UserContext";
//routing
import {Redirect, useHistory} from "react-router-dom";

export default function Login(){

	//The useHistory hook gives you access to the history instance that you may use to navigate / to access the location
	const history = useHistory();
	//useContext is a React hook used to unwrap our context. It will return the data passed as values by a provider (UserContext.Provider component in App.js)

	const {user,setUser} = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState(false);

	useEffect(()=>{
		if(email !== "" && password !== ""){
				setIsActive(true);
		}else{
				setIsActive(false);
		}
	},[email, password])

	function login(e){
		e.preventDefault();

		//fetch has 2 arg
		//1. URL from the server(routes)
		//2. optional object which contains additional information about our requests such as methods, headers, body, etc
		fetch("http://localhost:4000/users/login",{
			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res=>res.json())
		.then(data => {
			// console.log(data);
			//gives us the accessToken
			if(data.accessToken !== undefined){
				localStorage.setItem("accessToken", data.accessToken)
				setUser({accessToken:data.accessToken});

				Swal.fire({
				title: "Yaay!",
				icon: "success",
				text: `${email} has logged in to Zuitt Booking System`
			})		

				//get user's detail from our token
				fetch("http://localhost:4000/users/details",{
					headers:{
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res=>res.json())
				.then(data=>{
					console.log(data)

					if(data.isAdmin === true){
						localStorage.setItem("email",data.email);
						localStorage.setItem("isAdmin",data.isAdmin);
						setUser({
							email: data.email,
							isAdmin: data.isAdmin
						})
						//If admin, redirect the page to /courses
						//push(path) pushes a new entry onto the history stack
						history.push("/courses");
					}else{
						//If not admin, redirect to homepage
						history.push("/");
					}
				})

			}else{
				Swal.fire({
				title: "Oops!",
				icon: "error",
				text: "Something Went Wrong. Check your Credentials."
			})
			}
			setEmail("");
			setPassword("");
		})



		// Swal.fire({
		// 	title: "Login successful!",
		// 	icon: "success",
		// 	text: `${email} has logged in.`
		// })

		// setEmail("");
		// setPassword("");

		// //local storage allows us to save data within our browsers as strings
		// //setItem(), when passed a key name and value, will add that key to the given storage object, or update the key's value if it already exists
		// //setItem is used to store data in the localStorage in a string
		// //setItem("key",value)
		// localStorage.setItem("email",email);
		// setUser({email:email})
	}

	//Create a conditional statement that will redirect the user to the homepage when a user is logged in
	return(
		(user.accessToken !==null)
		? <Redirect to="/" />
		: <Fragment>
		<h1>Login</h1>
		<Form onSubmit={e=>login(e)}>
			<Form.Group>
				<Form.Label>Email address:</Form.Label>
				<Form.Control type="email" placeholder="Enter email" required value={email} 
				onChange={e=>setEmail(e.target.value)} />
			</Form.Group>

			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Enter your Password" required value={password}
				 onChange={e=>setPassword(e.target.value)} />
			</Form.Group>

			{isActive
			? <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
			: <Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
			}
		</Form>
		</Fragment>
		)
}
