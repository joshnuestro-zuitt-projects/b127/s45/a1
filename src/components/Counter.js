import {useState, useEffect, Fragment} from "react";
import {Button} from "react-bootstrap";
//By using this useEffect, you tell React that your components/elements need to do something after render. React will remember the function you passed(side-effect) and call it later after performing the DOM updates.

/*useEffect syntax
useEffect( ()=>, [] )

useEffect allows us to perform tasks/function on initial render:
- when the component is displayed for the first time

What allows us to control when our useEffect will run AFTER the initial render?
- we add an optional dependency ARRAY to control when useEffect will run, instead of it running on initial rener AND when states are updated, we can control the useEffect to run only when the state/s in the dependency array is updated.
*/

export default function Counter(){
	const [count, setCount] = useState(0);

	useEffect(()=>{
		document.title = `You clicked ${count} times`;
	}, [count])

	return(
		<Fragment>
			<p>You clicked {count}</p>
			<Button variant="primary" onClick={()=>setCount(count + 1)}>Click me</Button>
		</Fragment>
		)
}