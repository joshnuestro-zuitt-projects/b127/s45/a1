import {useState, useEffect} from "react";
import {Card,Button} from "react-bootstrap";
import PropTypes from "prop-types";
import	{Link} from "react-router-dom";

// export default function CourseCard(props){ // uses props.courseProp.something
export default function CourseCard({courseProp}){
	//Checks to see if the data was successfully passed
	console.log(courseProp);
	//console.log(typeof props);

	const { _id, name, description, price } = courseProp;

	//Use the state hook for this component to be able to store its state
	//States are used to keep track of information related to individual component
	//Syntax
		//const [getter, setter] = useState(initialGetterValue)
		//getter = stored initial(default value)
		//setter = updated value

	// const[count, setCount] = useState(0); 
	// const[seats, setSeats] = useState(10);

	//state hook that indicates the buton for enrollment
	// const [isOpen, setIsOpen] = useState(true);

	// useEffect(()=>{
	// 	if(seats === 0){
	// 		setIsOpen(false);
	// 	}
	// },[seats]) 
	//usually getter is put in the array
	// console.log(useState(0));

	// function enroll(){
	// 	setCount(count + 1);
	// 	console.log("Enrollees: " + count)

	// 	if(seats>0){
	// 	setSeats(seats - 1);
	// 	console.log("Seats: " + seats)
	// 	}else{
	// 	alert("No more seats available!");
	// 	}
	// }

	// function takeSeat(){

	// }

	return(
		<Card className="courseCard p-3">


			<Card.Body>
				<Card.Title><h3>{name}</h3></Card.Title>
{/*				<Card.Subtitle>Description</Card.Subtitle>
				<Card.Text>
							{description}
				</Card.Text>
				<Card.Subtitle>Price</Card.Subtitle>
				<Card.Text>₱ {price}</Card.Text>*/}

				<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>

			</Card.Body>
		</Card>
	
		)
}

//Check if the CourseCard Component is getting the correct prop types
//Proptypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next

CourseCard.propTypes={
	//The "shape" method is used to check if a prop object conforms to a specific "shape"
	courseProp: PropTypes.shape({
		//define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}

//			<Card.Body>
//				<Card.Title><h3>{props.courseProp.name}</h3></Card.Title>
//				<Card.Subtitle>Description</Card.Subtitle>
//				<Card.Text>
//							{props.courseProp.description}
//				</Card.Text>
//				<Card.Subtitle>Price</Card.Subtitle>
//				<Card.Text>₱ {props.courseProp.price}</Card.Text>
//
//				<Button variant="primary">Enroll</Button>
//			</Card.Body> -->


//			<Card.Body>
//				<Card.Title><h3>{courseProp.name}</h3></Card.Title>
//				<Card.Subtitle>Description</Card.Subtitle>
//				<Card.Text>
//							{courseProp.description}
//				</Card.Text>
//				<Card.Subtitle>Price</Card.Subtitle>
//				<Card.Text>₱ {courseProp.price}</Card.Text>

//				<Button variant="primary">Enroll</Button>
//			</Card.Body>