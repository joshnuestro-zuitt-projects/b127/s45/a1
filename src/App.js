import {useState, useEffect} from 'react';
import './App.css';
import AppNavbar from "./components/AppNavbar";
//pages
import Home from "./pages/Home";
import Courses from "./pages/Courses"
import Register from "./pages/Register";
import Login from "./pages/Login"
import Logout from "./pages/Logout"
import Error from "./pages/Error"
import SpecificCourse from "./pages/SpecificCourse"
// import Counter from "./components/Counter";

//routing components
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router-dom";

//Bootstrap
import {Container} from "react-bootstrap";

//React Context
import UserContext from "./UserContext";

//If you create UserProvider in it in the UserContext
// import { UserProvider } from './UserContext';
// <UserProvider value={ {user,setUser,unsetUser} }> will be used to wrap the components

/*
BrowserRouter component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser.

Switch component then declares with Route we can go to

Route component will render components within the switch container based on the defined route

'exact' property disables the partial matching for a route and makes sure that it only returns the route if the path is an exact match to the current url

If exact and path is missing, the Route component will make it undefined route and will be loaded into a specified component
*/

/*
React Context is a global state to the app. It is a way to make a particular data available to all the components no matter how they are nested. Context helps you to broadcast the data and changes happening to that data to all components.
*/

function App() {

  //add a state hook for user,
  //The getItem() method returns value of the specified Storage Object item
  // const [user, setUser] = useState({email:localStorage.getItem("email")});
  const [user, setUser] = useState({
    accessToken:localStorage.getItem("accessToken"),
    email: localStorage.getItem("email"),
    isAdmin: localStorage.getItem("isAdmin") === "true"

  });

  //Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear()
  }

  //Used to cheack if the user information is properly stored upon login and the localstorage information is cleared upon logout
  useEffect(()=>{
    console.log(user);
    console.log(localStorage);
  },[user])

  //We can pass data using UserContext in any route
  //Provider component allows consuming components to subscribe to context change
  return (
    <UserContext.Provider value={ {user,setUser,unsetUser} }>
      <Router>
        <AppNavbar />
        <Container>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/courses" component={Courses} />
            <Route exact path="/courses/:courseId" component={SpecificCourse} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/logout" component={Logout} />
            <Route component = {Error} />
          </Switch>
        </Container>
      </Router>
    </UserContext.Provider>
  );
}
        // <Counter />
export default App;

/*
NOTES
JSX Syntax
JSX, or Javascript XML is an extension to the syntax of JS. It alllows us to write HTML-like syntax within our React.js projects, and it includes JS features as well.

Fragment = div

Install the Js(Babel) linting for code readability
1. CTRL + Shift + P
2. Type "install" 
3. Select "Install Package Control"
4. Select "Package Control: Install Package"
5. Type "babel" and install it
*/