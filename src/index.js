import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

//Import the Bootstrap CSS
import "bootstrap/dist/css/bootstrap.min.css";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// const name = "Peter";
// const user = {
//   firstName: "Jasmine",
//   lastName: "Timberlake"
// }
// const hellow = `
//  _   _      _ _       
// | | | | ___| | | ___  
// | |_| |/ _ \\ | |/ _ \\ 
// |  _  |  __/ | | (_) |
// |_| |_|\\___|_|_|\\___/ `


// // function formatName(user){
// //   return user.firstName + " " + user.lastName;
// // }

// const formatName=(user)=>{
//   return user.firstName + " " + user.lastName;
// }

// // const element = <p>Hello, {name}</p> //Hello, Peter
// {/*const element = <h1>Hello, {formatName(user)}</h1> //Hello, Jasmine Timberlake*/}
// const element = <pre>{hellow}{formatName(user)}</pre>


// ReactDOM.render(element, document.getElementById("root"));